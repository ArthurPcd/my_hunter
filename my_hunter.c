/*
** EPITECH PROJECT, 2021
** my_hunter
** File description:
** my_hunter
*/
int start_bool = 0;
#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include <stdbool.h>
#include "stdlib.h"
#include <unistd.h>
void my_putchar(char c);
int my_putstr(char const *str);
int my_intlen(int num);
char *nb_to_str(int num);

void game() {
//---------------------------------------------------------------------------------------------//
//Working initialising variables
//---------------------------------------------------------------------------------------------//
    int count = 3;
    int pause_count = 1;
    int counter_points = 0;
    int money_counter = 0;
    int points_vel = 0;
    int numb_birds = 1;
    int index;
    int pause_bool = 0;
    double multiplier = 0.5;
    double px_mouse;
    double py_mouse;
    double px = 0;
    double px_birds = 0;
    double py;
    index = rand();
    if (index < 900 && index > 40) {
        py = index;
    }
    else
        py = (index % 900) + 40;
    double velocity = 0.225;
    double start_px = 800;
    double start_py = 425;
    double settings_px = 0;
    double settings_py = 0;
    double exit_px = 805;
    double exit_py = 900;
    double exit_px2 = 1405;
    sfVector2f invisible = {-900, -900};
    //---------------------------------------------------------------------------------------------//
    //Main window create
    //---------------------------------------------------------------------------------------------//

    sfRenderWindow *window;
    sfVideoMode video_mode = {1920, 1080, 64};
    sfTexture *image = sfTexture_createFromFile("sources/backgrounds/background.png", NULL);
    sfVector2f size = {1920, 1080};
    sfRectangleShape *background = sfRectangleShape_create();
    sfRectangleShape_setSize(background, size);
    sfRectangleShape_setTexture(background, image, false);

    sfSprite *pointeur = sfSprite_create();
    sfTexture *pointeur_image = sfTexture_createFromFile("sources/sprites/cursor/pointeur.png", NULL);
    sfTexture *hand = sfTexture_createFromFile("sources/sprites/cursor/hand.png", NULL);
    //---------------------------------------------------------------------------------------------//
    // Pause window create
    //---------------------------------------------------------------------------------------------//
    //    sfRenderWindow *pause_window;
    //    sfVideoMode pause_vid = {448, 364, 32};
    sfSprite *pause = sfSprite_create();
    sfVector2f pause_pos = {700, 325};
    sfTexture *pause_image = sfTexture_createFromFile("sources/backgrounds/pause_bckground.png", NULL);

    sfSprite *exitp_button = sfSprite_create();
    sfVector2f exitp_pos = {795, 565};

    sfSprite *resume = sfSprite_create();
    sfVector2f resume_pos = {795, 415};
    sfTexture *resume_image = sfTexture_createFromFile("sources/sprites/game/resume_sprite.png", NULL);
    //---------------------------------------------------------------------------------------------//
    //Starting Menu
    //---------------------------------------------------------------------------------------------//

    sfRenderWindow *start_window;
    sfVideoMode start_vid = {1920, 1080, 64};
    sfTexture *start_image = sfTexture_createFromFile("sources/backgrounds/start_bckground.png", NULL);
    sfRectangleShape *startbackground = sfRectangleShape_create();
    sfVector2f start_size = {1920, 1080};
    sfRectangleShape_setSize(startbackground, start_size);
    sfRectangleShape_setTexture(startbackground, start_image, false);

    sfRenderWindow *params_window;
    sfVideoMode params_vid = {1920, 1080, 64};
    sfTexture *paramsimage = sfTexture_createFromFile("sources/backgrounds/help_background.png", NULL);
    sfRectangleShape *paramsbackground = sfRectangleShape_create();
    sfVector2f params_size = {1920, 1080};
    sfRectangleShape_setSize(paramsbackground, params_size);
    sfRectangleShape_setTexture(paramsbackground, paramsimage, false);

    //create game start button
    sfSprite *start_button = sfSprite_create();
    sfVector2f sbutton_position = {start_px, start_py};
    sfTexture *start_texture = sfTexture_createFromFile("sources/sprites/game/play_sprite.png", NULL);

    //create game setting button
    sfSprite *setting_button = sfSprite_create();
    sfVector2f setting_position = {settings_px, settings_py};
    sfTexture *setting_texture = sfTexture_createFromFile("sources/sprites/game/game_help.png", NULL);

    //create game exit button
    sfSprite *exit_button = sfSprite_create();
    sfVector2f exit_position = {exit_px, exit_py};
    sfVector2f exit_position2 = {exit_px2, exit_py};
    sfTexture *exit_texture = sfTexture_createFromFile("sources/sprites/game/exit_game_sprite.png", NULL);
    sfTexture *exit_texture2 = sfTexture_createFromFile("sources/sprites/game/exit_pause_sprite.png", NULL);

    sfMusic *farwest = sfMusic_createFromFile("sources/sounds/music_start.ogg");
    sfMusic *shoot = sfMusic_createFromFile("sources/sounds/shoot.ogg");
    sfMusic *oof = sfMusic_createFromFile("sources/sounds/oof.ogg");
    //---------------------------------------------------------------------------------------------//
    //Bird Sprite create
    //---------------------------------------------------------------------------------------------//

    sfSprite *bird = sfSprite_create();
    sfTexture *bird_texture = sfTexture_createFromFile("sources/sprites/birds/bird_1.png", NULL);
    sfTexture *bird_texture2 = sfTexture_createFromFile("sources/sprites/birds/bird_2_6.png", NULL);
    sfTexture *bird_texture3 = sfTexture_createFromFile("sources/sprites/birds/bird_3.png", NULL);
    sfTexture *bird_texture4 = sfTexture_createFromFile("sources/sprites/birds/bird_3_5.png", NULL);
    sfTexture *bird_texture5 = sfTexture_createFromFile("sources/sprites/birds/bird_4.png", NULL);
    sfTexture *bird_texture6 = sfTexture_createFromFile("sources/sprites/game/explosion.png", NULL);
    sfSprite_setTexture(bird, bird_texture, true);

    //---------------------------------------------------------------------------------------------//
    //Text create and modify (lives_counter counter)
    //---------------------------------------------------------------------------------------------//

    sfText *text;
    sfText *lives_counter;
    sfFont *font;
    sfFont *pixel_font;
    sfVector2f pos_lives_counter = {140, 0};

    sfText *point;
    sfText *point_count;
    sfVector2f pos_point = {260, 0};
    sfVector2f pos_points_counter = {420, 0};

    sfText *money;
    sfText *moneycount;
    sfVector2f money_pos = {1600, 0};
    sfVector2f money_counter_pos = {1745, 0};

//    sfText *moula;
//    sfVector2f moula_pos = {1600, 0};

    /* Create a graphical text to display */

    font = sfFont_createFromFile("sources/fonts/arial.ttf");
    pixel_font = sfFont_createFromFile("sources/fonts/pixel.ttf");
    if (!font)
        return;
    text = sfText_create();
    sfText_setFillColor(text, sfBlack);
    sfText_setString(text, "Lives :");
    sfText_setFont(text, pixel_font);
    sfText_setCharacterSize(text, 35);

    lives_counter = sfText_create();
    sfText_setFillColor(lives_counter, sfRed);
    sfText_setPosition(lives_counter, pos_lives_counter);
    sfText_setString(lives_counter, nb_to_str(count));
    sfText_setFont(lives_counter, pixel_font);
    sfText_setCharacterSize(lives_counter, 35);

    point = sfText_create();
    sfText_setFillColor(point, sfBlack);
    sfText_setPosition(point, pos_point);
    sfText_setString(point, "Points :");
    sfText_setFont(point, pixel_font);
    sfText_setCharacterSize(point, 35);

    point_count = sfText_create();
    sfText_setFillColor(point_count, sfBlue);
    sfText_setPosition(point_count, pos_points_counter);
    sfText_setString(point_count, nb_to_str(counter_points));
    sfText_setFont(point_count, pixel_font);
    sfText_setCharacterSize(point_count, 35);

    money = sfText_create();
    sfText_setFillColor(money, sfBlack);
    sfText_setPosition(money, money_pos);
    sfText_setString(money, "Money :         $");
    sfText_setFont(money, pixel_font);
    sfText_setCharacterSize(money, 35);

    moneycount = sfText_create();
    sfText_setFillColor(moneycount, sfGreen);
    sfText_setPosition(moneycount, money_counter_pos);
    sfText_setString(moneycount, nb_to_str(money_counter));
    sfText_setFont(moneycount, pixel_font);
    sfText_setCharacterSize(moneycount, 35);

//    moula = sfText_create();
//    sfText_setFillColor(moula, sfBlack);
//    sfText_setPosition(moula, moula_pos);
//    sfText_setString(moula, "+   $");
//    sfText_setFont(moula, pixel_font);
//    sfText_setCharacterSize(moula, 25);

    //---------------------------------------------------------------------------------------------//
    //Initialising all.
    //---------------------------------------------------------------------------------------------//

    if (sfMusic_getStatus(farwest) == sfPlaying || start_bool == 0) {
        sfMusic_play(farwest);
        sfMusic_setVolume(farwest, 75);
    }
    start_window = sfRenderWindow_create(start_vid, "my_hunter", sfClose | sfResize, NULL);
    while (sfRenderWindow_isOpen(start_window)) {
        px_mouse = (sfMouse_getPosition((sfWindow *) start_window).x - 60);
        py_mouse = (sfMouse_getPosition((sfWindow *) start_window).y - 53);
        sfVector2f position_souris = {px_mouse, py_mouse};
        sfRenderWindow_setMouseCursorVisible((sfRenderWindow *) start_window, 0);
        sfRenderWindow_drawSprite(start_window, pointeur, NULL);
        sfSprite_setTexture(pointeur, hand, false);
        sfSprite_setPosition(pointeur, position_souris);
        sfRenderWindow_display(start_window);
        sfRenderWindow_drawRectangleShape(start_window, startbackground, NULL);

        //display start game button
        sfRenderWindow_drawSprite(start_window, start_button, NULL);
        sfSprite_setPosition(start_button, sbutton_position);
        sfSprite_setTexture(start_button, start_texture, true);

        //display game setting button
        sfRenderWindow_drawSprite(start_window, setting_button, NULL);
        sfSprite_setPosition(setting_button, setting_position);
        sfSprite_setTexture(setting_button, setting_texture, true);

        //display game exit button
        sfRenderWindow_drawSprite(start_window, exit_button, NULL);
        sfSprite_setPosition(exit_button, exit_position);
        sfSprite_setTexture(exit_button, exit_texture, true);

        if (sfMouse_isButtonPressed(sfMouseLeft)) {
            if (sfMouse_isButtonPressed(sfMouseLeft) &&
                sfMouse_getPosition((sfWindow *) start_window).x > start_px &&
                sfMouse_getPosition((sfWindow *) start_window).x < start_px + 259 &&
                sfMouse_getPosition((sfWindow *) start_window).y > start_py &&
                sfMouse_getPosition((sfWindow *) start_window).y < (start_py + 150)) {
                start_bool = 3;
                window = sfRenderWindow_create(video_mode, "my_hunter", sfClose | sfResize, NULL);
                sfRenderWindow_close(start_window);
                sfTexture_destroy(start_image);
                sfRectangleShape_destroy(startbackground);
            }
            if (sfMouse_isButtonPressed(sfMouseLeft) &&
                sfMouse_getPosition((sfWindow *) start_window).x > settings_px &&
                sfMouse_getPosition((sfWindow *) start_window).x < settings_px + 125 &&
                sfMouse_getPosition((sfWindow *) start_window).y > settings_py &&
                sfMouse_getPosition((sfWindow *) start_window).y < settings_py + 125) {
                sfRenderWindow_close(start_window);
                sfTexture_destroy(start_image);
                sfRectangleShape_destroy(startbackground);
                sfMusic_destroy(farwest);
                sfMusic_destroy(shoot);
                sfMusic_destroy(oof);
                params_window = sfRenderWindow_create(params_vid, "help", sfClose | sfResize, NULL);
                while (sfRenderWindow_isOpen(params_window)) {
                    sfRenderWindow_display(params_window);
                    sfRenderWindow_drawRectangleShape(params_window, paramsbackground, NULL);
                    //display game exit button
                    sfRenderWindow_drawSprite(params_window, exit_button, NULL);
                    sfSprite_setPosition(exit_button, exit_position2);
                    sfSprite_setTexture(exit_button, exit_texture2, true);
                    if (sfMouse_isButtonPressed(sfMouseLeft) &&
                        sfMouse_getPosition((sfWindow *) params_window).x > exit_px2 &&
                        sfMouse_getPosition((sfWindow *) params_window).x < exit_px2 + 300 &&
                        sfMouse_getPosition((sfWindow *) params_window).y > exit_py &&
                        sfMouse_getPosition((sfWindow *) params_window).y < exit_py + 86) {
                        sfRenderWindow_close(params_window);
                        sfTexture_destroy(paramsimage);
                        sfRectangleShape_destroy(paramsbackground);
                        game();
                        return;
                    }
                }
            }
            if (sfMouse_isButtonPressed(sfMouseLeft) &&
                sfMouse_getPosition((sfWindow *) start_window).x > exit_px &&
                sfMouse_getPosition((sfWindow *) start_window).x < exit_px + 300 &&
                sfMouse_getPosition((sfWindow *) start_window).y > exit_py &&
                sfMouse_getPosition((sfWindow *) start_window).y < exit_py + 86) {
                sfRenderWindow_close(start_window);
                sfTexture_destroy(start_image);
                sfRectangleShape_destroy(startbackground);
                sfMusic_destroy(shoot);
                sfMusic_destroy(oof);
                sfMusic_destroy(farwest);
                return;
            }
        }
    }
    if (start_bool == 3) {
        sfMusic_destroy(farwest);
        start_bool = 5;
    }
    while (sfRenderWindow_isOpen(window)) {
        px_mouse = (sfMouse_getPosition((sfWindow *) window).x - 60);
        py_mouse = (sfMouse_getPosition((sfWindow *) window).y - 53);
        sfVector2f position_souris = {px_mouse, py_mouse};
        sfWindow_setMouseCursorVisible((sfWindow *) window, 0);
        sfRenderWindow_display(window);
        sfRenderWindow_drawRectangleShape(window, background, NULL);
        sfRenderWindow_drawSprite(window, bird, NULL);
        sfRenderWindow_drawSprite(window, pause, NULL);
        sfRenderWindow_drawSprite(window, exitp_button, NULL);
        sfRenderWindow_drawSprite(window, resume, NULL);;
        sfRenderWindow_drawText(window, text, NULL);
        sfRenderWindow_drawText(window, lives_counter, NULL);
        sfRenderWindow_drawText(window, point, NULL);
        sfRenderWindow_drawText(window, point_count, NULL);
        sfRenderWindow_drawText(window, money, NULL);
        sfRenderWindow_drawText(window, moneycount, NULL);
        sfVector2f position = {px, py};
        sfSprite_setPosition(bird, position);
        sfRenderWindow_drawSprite(window, pointeur, NULL);
        sfSprite_setTexture(pointeur, pointeur_image, false);
        sfSprite_setPosition(pointeur, position_souris);
        if (sfKeyboard_isKeyPressed(sfKeyEscape)) {
            resume_pos.x = 795;
            resume_pos.y = 415;
            exitp_pos.x = 795;
            exitp_pos.y = 565;
            pause_bool = 1;
            pause_count = 0;
            sfSprite_setTexture(pause, pause_image, false);
            sfSprite_setPosition(pause, pause_pos);

            sfSprite_setTexture(exitp_button, exit_texture, false);
            sfSprite_setPosition(exitp_button, exitp_pos);

            sfSprite_setTexture(resume, resume_image, false);
            sfSprite_setPosition(resume, resume_pos);
        } else if (pause_bool == 1 &&
                   (sfKeyboard_isKeyPressed(sfKeyA) ||
                    (sfMouse_isButtonPressed(sfMouseLeft) &&
                     (sfMouse_getPosition((sfWindow *) window).x > resume_pos.x &&
                      sfMouse_getPosition((sfWindow *) window).x < resume_pos.x + 300 &&
                      sfMouse_getPosition((sfWindow *) window).y > resume_pos.y &&
                      sfMouse_getPosition((sfWindow *) window).y < resume_pos.y + 86)))) {
            sfSprite_setPosition(pause, invisible);
            sfSprite_setPosition(exitp_button, invisible);
            sfSprite_setPosition(resume, invisible);
            sfMusic_stop(shoot);
            resume_pos.x = -800;
            resume_pos.y = -800;
            exitp_pos.x = -800;
            exitp_pos.y = -800;
            pause_count = 1;
            pause_bool == 0;
        } else if (pause_bool == 1 &&
                   (sfKeyboard_isKeyPressed(sfKeyQ) ||
                    (sfMouse_isButtonPressed(sfMouseLeft) &&
                     (sfMouse_getPosition((sfWindow *) window).x > exitp_pos.x &&
                      sfMouse_getPosition((sfWindow *) window).x < exitp_pos.x + 300 &&
                      sfMouse_getPosition((sfWindow *) window).y > exitp_pos.y &&
                      sfMouse_getPosition((sfWindow *) window).y < exitp_pos.y + 86)))) {
            my_putstr("Game Ended");
            my_putstr("\nYour score : ");
            my_putstr(nb_to_str(counter_points));
            sfMusic_destroy(shoot);
            sfMusic_destroy(oof);
            if (start_bool == 3)
                sfMusic_destroy(farwest);
            return;
        }
        if (pause_count == 1) {
            px += velocity;
            px_birds += velocity;

            if (px > 1920) {
                sfMusic_stop(oof);
                if (sfMusic_getStatus(oof) == sfPaused || sfMusic_getStatus(oof) == sfStopped)
                    sfMusic_play(oof);
                sfMusic_stop(shoot);
                px = -260;
                count--;
                sfText_setString(lives_counter, nb_to_str(count));
            }
            if (px_birds < 50 && px_birds > -75) {
                sfSprite_setTexture(bird, bird_texture, true);
            } else if (px_birds < 100 && px_birds > 50) {
                sfSprite_setTexture(bird, bird_texture2, true);
            } else if (px_birds < 150 && px_birds > 100) {
                sfSprite_setTexture(bird, bird_texture3, true);
            } else if (px_birds < 200 && px_birds > 150) {
                sfSprite_setTexture(bird, bird_texture4, true);
            } else if (px_birds < 250 && px_birds > 200) {
                sfSprite_setTexture(bird, bird_texture5, true);
            } else if (px_birds < 300 && px_birds > 250) {
                sfSprite_setTexture(bird, bird_texture4, true);
            } else if (px_birds < 350 && px_birds > 300) {
                sfSprite_setTexture(bird, bird_texture3, true);
            } else if (px_birds < 400 && px_birds > 350) {
                sfSprite_setTexture(bird, bird_texture2, true);
                px_birds = 0;
            }
            if (points_vel == 500) {
                velocity += 0.125;
                multiplier += 0.25;
                points_vel = 0;
                numb_birds += 1;
            }
            if (sfMouse_isButtonPressed(sfMouseLeft)) {
                if (sfMouse_getPosition((sfWindow *) window).x > px &&
                    sfMouse_getPosition((sfWindow *) window).x < px + 125 &&
                    sfMouse_getPosition((sfWindow *) window).y > py &&
                    sfMouse_getPosition((sfWindow *) window).y < py + 75) {
                    if (sfMusic_getStatus(oof) == sfPlaying) {
                        sfMusic_stop(oof);
                    }
                    sfMusic_play(shoot);
                    counter_points += 100;
                    points_vel += 100;
                    money_counter += 100 * multiplier;
                    sfText_setString(point_count, nb_to_str(counter_points));
                    sfText_setString(moneycount, nb_to_str(money_counter));
                    index = rand();
                    if (index < 900 && index > 40) {
                        py = index;
                    } else
                        py = (index % 900) + 40;
                    px = -250;
                }
            }
            if (count == 0) {
                sfText_setString(lives_counter, nb_to_str(count));
                sfText_destroy(text);
                sfFont_destroy(font);
                sfTexture_destroy(image);
                sfRectangleShape_destroy(background);
                sfRenderWindow_destroy(window);
                my_putstr("Game Over");
                my_putstr("\nYour score : ");
                my_putstr(nb_to_str(counter_points));
                sfMusic_destroy(shoot);
                sfMusic_destroy(oof);
                if (start_bool == 3)
                    sfMusic_destroy(farwest);
                return;
            }
        }
    }
}
void helper()
{
    my_putstr("\033[0;33mWelcome on The My Hunter Farwest Edition ! \n");
    my_putstr("\033[0;35mHere is the help menu\n");
    my_putstr("\033[0;34mTo start the game do ");
    my_putstr("\033[0;31m\"./my_hunter\"");
    my_putstr("\033[0;34m command in your terminal \n");
    my_putstr("Then you can press the ");
    my_putstr("\033[0;31m\"HELP\"");
    my_putstr("\033[0;34m button \"at the top left to see the rules ! \n");
    my_putstr("\033[0;33mGood Luck Have Fun ! :)");
}
int main(int ac, char **av)
{
    if (ac > 1) {
        if (av[1][0] == '-' && av[1][1] == 'h' && av[1][2] == 0) {
            helper();
        } else
            write(2, "\033[0;31mError flag not recognised", 33);
        return 0;
        }
    else
        game();
    return 0;
}
